class ConnectionError extends Error {
    constructor(message) {
        super('Impossible de se connecter à l\'API, erreur :'+message);
    }
}