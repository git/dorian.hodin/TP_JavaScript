export default {
    props: {
        teamAEdit: {
            require: true
        },
    },
    data: function() {
        return {
            id:'',
            name: '',
            description: '',
            errorMessage: '',
            errorColor: ERROR_COLOR
        }
    },
    methods: {
        addTeam: function () {
            try {

                if (this.teamAEdit != null) {
                    this.id = this.teamAEdit.id
                    this.name = this.teamAEdit.name
                    this.description = this.teamAEdit.description
                }

                this.errorMessage = '';
                if (!this.id) {
                    throw new RequiredFieldError("ID");
                }
                if (!this.name) {
                    throw new RequiredFieldError("Name");
                }
                if (!this.description){
                    throw new RequiredFieldError("Description");
                }
                if (this.description.length < DESCRIPTION_MINIMAL_SIZE) {
                    throw new StringSize("Description", DESCRIPTION_MINIMAL_SIZE)
                }
                if (this.name.length < NAME_MINIMAL_SIZE) {
                    throw new StringSize("Name", NAME_MINIMAL_SIZE)
                }

                const team = { id: this.id, name: this.name, description: this.description };




                if (this.teamAEdit != null) {
                    this.$emit('teamModifie', team);
                } else {

                    this.$emit('teamAjoute', team);
                }

                this.id = '';
                this.name = '';
                this.description = '';

            } catch (error) {
                if (error instanceof RequiredFieldError) {
                    this.errorMessage=error
                }
                if (error instanceof StringSize) {
                    this.errorMessage = error
                }
                return null;
            }
        }
    },
    template: `<section>
                <form @submit.prevent>
                    <div v-bind:style="{ color: errorColor}">{{ errorMessage }}</div>
                    <div>
                        <label>ID</label><br/>
                        <p v-if=this.teamAEdit><input type="text" v-model="this.teamAEdit.id" /></p>
                        <p v-else><input type="text" v-model="this.id" /></p>
                    </div>
                    <div>
                        <label>Name</label><br/>
                        <p v-if=this.teamAEdit><input type="text" v-model="this.teamAEdit.name" /></p>
                        <p v-else><input type="text" v-model="this.name" /></p>
                    </div>
                    <div>
                        <label>Description</label><br/>
                        <p v-if=this.teamAEdit><textarea v-model="this.teamAEdit.description"/></p>
                        <p v-else><textarea v-model="this.description" /></p>
                    </div>
                    <input type="submit" value="Create Team" @click="addTeam"/>
                </form>
            </section>`
}