export default {
    props: {
        id: {
            type :String,
            require : true
        },
        name: {
            type :String,
            require : true
        },
        description: {
            type :String,
            require : true
        }
    },
    data: function () {
        return {
            isEdit: false
        }
    },
    methods: {
        editTeam: function () {
            this.isEdit = !this.isEdit;
            const team = { id: this.id, name: this.name, description: this.description };
            this.$emit('teamEditer', team);

        }
    },
    computed: {
        description20(){
            if (this.description.length > 20) {
                return this.description.substring(0, 20) + '...';
              } else {
                return this.description;
              }
            }
    },
    template: `
        <div class="card">
            <span>ID of the Team : {{ id }}</span>
            <p>Name : {{ name }}</p>
            <p>Description : {{ description20 }}</p>
            <input type="submit" value="Edit" @click="editTeam"/>
        </div>
    `
}