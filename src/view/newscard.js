export default {
    props: {
        title: {
            type :String,
            require : true
        },
        publishedAt: {
            type :Date,
            require : true
        }
    },
    template: `
        <div class="card">
            <h1>OUI</h1>
            <span>Title : {{ title }}</span>
            <p>Published At : {{ publishedAt }}</p>
        </div>
    `
}