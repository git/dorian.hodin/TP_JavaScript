import teamAdd from './team-add.js';
import teamCard from './teamcard.js';


export default {
    data: function() {
        return {
            allTeam: [],
            teamAEdit: null
        }
    },
    methods: {
        recupTeam: function (team) {
            this.allTeam.push(team);
            this.$emit('addTeam', team);
        },

        editParam: function (team) {
            this.teamAEdit=team
        },

        updateTeam: function (team) {
            for (var i = 0; i < this.allTeam.length; i++) {
                if (this.allTeam[i].id == this.teamAEdit.id) {
                    this.allTeam[i].name = team.name
                    this.allTeam[i].description=team.description
                }
            }
            this.teamAEdit = null
        },

        exportTeam: function(){
            console.log("Team to JSON :")
            console.log(JSON.stringify(this.allTeam));
        }


    },
    components: {
        teamCard,
        teamAdd
    },
    template: `
        <section>
            <h2>News form</h2>
            <div class="team">
                <teamAdd :teamAEdit="this.teamAEdit" v-on:teamAjoute="recupTeam" v-on:teamModifie="updateTeam"></teamAdd>
            </div>
            <teamCard v-for="team in allTeam"
                      :id="team.id"
                      :name="team.name"
                      :description="team.description"
                      v-on:teamEditer="editParam">
            </teamCard>
            <button @click="exportTeam">Export</button>
        </section>
    `
}
