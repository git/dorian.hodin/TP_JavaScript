export default {
    props: {
        linkHome: {
            type: String,
            default: '#'
        },
        linkTeam:{
            type: String,
            default: '#'
        },
        linkResults: {
            type: String,
            default: '#'
        }
    },
    template:`
        <ul>
            <li onclick="showHome()"><a :href="linkHome">Home</a></li>
            <li onclick="showTeam()"><a :href="linkTeam">Team</a></li>
            <li onclick="showResults()"><a :href="linkResults">Results</a></li>
        </ul>
    `
};