export default {
    data: function(){
        return {
            homeTeam: null,
            homeScore: 0,
            awayTeam: null,
            awayScore: 0,
            allTeam: [],
            errorMessage: '',
            errorColor: ERROR_COLOR
          }
    },
    methods: {
        sendResults: function () {
          fetch("http://www.post-result.com", {
              method: "POST",
              body: JSON.stringify({
                  homeTeam: this.homeTeam,
                  homeScore: this.homeScore,
                  awayTeam: this.awayTeam,
                  awayScore: this.awayScore
              })
              }).then(response =>{
                  if (!response.ok){
                      throw new ConnectionError(error.message);
                  }
                  console.log("Ok, data send successfully");
          }).catch(error => {
              this.errorMessage=error
              return null;
          });  
        },
  
        getTeam: function (team) {
          this.allTeam.push(team);
        },
    },
    template: `
    <div>
    <h2>Match :</h2>
    <form @submit.prevent>
      <div>
        <label for="home-team">Home team:</label>
        <select id="home-team" v-model="homeTeam">
          <option selected disabled >Select a Team</option>
          <option v-for="team in allTeam">{{ team.name }}
          </option>
        </select>
      </div>
      <div>
        <label for="home-score">Home score:</label>
        <input type="number" id="home-score" v-model="homeScore" min="0" required>
      </div>
      <div>
        <label for="away-team">Away team:</label>
        <select id="away-team" v-model="awayTeam">
          <option selected disabled>Select a Team</option>
          <option v-for="team in allTeam">{{ team.name }}
          </option>
        </select>
      </div>
      <div>
        <label for="away-score">Away score:</label>
        <input type="number" id="away-score" v-model="awayScore" min="0" required>
      </div>
      <input type="submit" value="Send result" @click="sendResults"/>
      <div v-bind:style="{ color: errorColor}">{{ errorMessage }}</div>
    </form>
  </div>
    `
}