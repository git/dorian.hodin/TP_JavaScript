class ApiService{

    async loadFromApi(url,method) {
        const headers = {
            method: method,
        };
        let a = await fetch(url, headers).then(response => {
            if (!response.ok){
                throw new Error('Une erreur est survenue durant l\'appel HTTP');
            }
            return response.json();
        });
        return a;
    }
}